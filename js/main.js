

$(document).ready(function () {

	// menu
	$(".toggle-menu").click(function(){
		$(this).toggleClass("active")
		$("body").toggleClass("active-menu")
		$(".wrap-menu").toggleClass("active")
	})
	$(document).mouseup(function (e) {
		var container = $("header#header");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
				$(".toggle-menu").removeClass("active")
				$("body").removeClass("active-menu")
				$(".wrap-menu").removeClass("active")
		}
	});
	// 
	$(".toggle-item-menu").click(function(){
		$(this).siblings(".box-menu-child").slideToggle(400)
	})
});


// fix header

$(window).on('scroll', function () {
	if ($(window).scrollTop()) {
		$('#header').addClass('active');
	} else {
		$('#header').removeClass('active')
	};
});





// custome lomo 


$('#banner-main .owl-carousel').owlCarousel({
	nav: true,
	dots: false,
	autoplay: true,
	autoplayTimeout: 8000,
	autoplayHoverPause: true,
	margin: 0,
navText: ["<span class='icon icon-prev'></span>","<span class='icon icon-prev'></span>"],
	responsive: {
		0: {
			items: 1,
			loop: ($('#banner-main .owl-carousel .item').length > 1) ? true : false,
		},
	}
});
$('#slider-doi-tac').owlCarousel({
	nav: true,
	dots: false,
	autoplay: true,
	autoplayTimeout: 8000,
	autoplayHoverPause: true,
	margin: 20,
	responsive: {
		0: {
			items: 2,
			loop: ($('#slider-doi-tac .item').length > 2) ? true : false,
		},
		540: {
			items: 3,
			loop: ($('#slider-doi-tac .item').length > 3) ? true : false,
		},
		768: {
			items: 4,
			loop: ($('#slider-doi-tac .item').length > 4) ? true : false,
		},
		1024: {
			items: 6,
			loop: ($('#slider-doi-tac .item').length > 6) ? true : false,
		},
	}
});
// 

function resizeImage() {
	let arrClass = [
		{ class: 'resize-new-big', number: (200 / 355) }, 
		{ class: 'resize-new', number: (75 / 100) }, 
	];
	for (let i = 0; i < arrClass.length; i++) {
		if($("." + arrClass[i]['class']).length){
			let width = document.getElementsByClassName(arrClass[i]['class'])[0].offsetWidth;
			$("." + arrClass[i]['class']).css('height', width * arrClass[i]['number'] + 'px');
		}
	}
}

    
resizeImage();
new ResizeObserver(() => {
	resizeImage();
	
}).observe(document.body)

// 

